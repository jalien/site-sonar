#!/bin/bash

# Checks if running in docker

cgroup=`grep cpuset /proc/1/cgroup`

if [[ ${cgroup} == *"docker"* ]]; then
	echo "{ \"RUNNING_IN\" : \"Docker\" }"
	exit 1
else 
	# Checks if running in singularity

	env=`env | grep SINGULARITY`
	if [[ ${env} ]]; then
		echo "{ \"RUNNING_IN\" : \"Singularity\" }"
		exit 2
	else 
		echo "{ \"RUNNING_IN\" : \"no container\" }"
	fi
fi

exit 0
