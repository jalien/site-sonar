#!/bin/bash

# Print whether overlay is enabled

OVERLAY_ENABLED=$(grep "^enable overlay" /etc/singularity/singularity.conf | cut -d "=" -f 2 | xargs)

if [[ $? == 0 ]]
then
    echo "{ \"OVERLAY_ENABLED\" : \"$OVERLAY_ENABLED\" }"
else 
    exit_code=$?
    echo "{ \"OVERLAY_ENABLED\" : \"\" }"
    exit $exit_code
fi
