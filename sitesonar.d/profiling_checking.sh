#!/bin/bash

# Checks whether OS configurations allow profiling

PERF_EVENT_PARANOID_PATH="/proc/sys/kernel/perf_event_paranoid"
PTRACE_SCOPE_PATH="/proc/sys/kernel/yama/ptrace_scope"

perf_event_paranoid_value=$(cat $PERF_EVENT_PARANOID_PATH)
exitcode_1=$?
if [ $exitcode_1 -ne 0 ]; then
    perf_event_paranoid_value=""
fi


ptrace_scope_value=$(cat $PTRACE_SCOPE_PATH)
exitcode_2=$?

if [ $exitcode_2 -ne 0 ]; then
    ptrace_scope_value=""
fi


if [ $exitcode_1 -ne 0 ] || [ $exitcode_2 -ne 0 ]; then         # if any of the configs are not accessible
    vtune_profiling_enabled=false
else

    if [ $perf_event_paranoid_value -ne 0 ] || [ $ptrace_scope_value -ne 0 ]; then  # if any of the configs are not 0, profiling can not be performed
        vtune_profiling_enabled=false
    else
        vtune_profiling_enabled=true
    fi

fi

JSON_OUTPUT="{ \"PERF_EVENT_PARANOID\" : \"$perf_event_paranoid_value\", \"PTRACE_SCOPE\" : \"$ptrace_scope_value\", \"VTUNE_PROFILING_ENABLED\" : $vtune_profiling_enabled }"

echo "$JSON_OUTPUT"

