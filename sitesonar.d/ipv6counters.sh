#!/bin/bash

TOTALINV4=0
TOTALOUTV4=0

TOTALINV6=0
TOTALOUTV6=0

RESPONSE="{ \"Interfaces\": {"

while read IF TOTALIN TOTALOUT; do
    IF6IN=0
    IF6OUT=0

    if [ ${#RESPONSE} -gt 30 ]; then
	RESPONSE="$RESPONSE,"
    fi

    RESPONSE="$RESPONSE \"$IF\": {"

    while read KEY VALUE; do
	if [[ "$KEY" =~ ^Ip6In.* ]]; then
	    IF6IN=$((IF6IN+VALUE))
	else
	    IF6OUT=$((IF6OUT+VALUE))
	fi

	RESPONSE="$RESPONSE \"$KEY\":$VALUE,"
    done < <(grep Octets /proc/net/dev_snmp6/$IF 2>/dev/null)

    TOTALINV6=$((TOTALINV6+IF6IN))
    TOTALOUTV6=$((TOTALOUTV6+IF6OUT))

    TOTALINV4=$((TOTALINV4+TOTALIN-IF6IN))
    TOTALOUTV4=$((TOTALOUTV4+TOTALOUT-IF6OUT))

    RESPONSE="$RESPONSE \"IPv6TotalIn\": $IF6IN, \"IPv6TotalOut\": $IF6OUT, \"TotalInterfaceIn\": $TOTALIN, \"TotalInterfaceOut\": $TOTALOUT, \"IPv4TotalIn\": $((TOTALIN-IF6IN)), \"IPv4TotalOut\": $((TOTALOUT-IF6OUT))}"

done < <(cat /proc/net/dev | awk '{print $1 " " $2 " " $10}' | tail -n +3 | sed 's/://g')

RESPONSE="$RESPONSE}"

RESPONSE="$RESPONSE, \"Summary\": { \"IPv4In\": $TOTALINV4, \"IPv4Out\": $TOTALOUTV4, \"IPv6In\": $TOTALINV6, \"IPv6Out\": $TOTALOUTV6}"

echo "$RESPONSE}"
