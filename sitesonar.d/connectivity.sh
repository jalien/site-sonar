#!/bin/bash

# voboxalice1.cern.ch on both stacks
TARGET4="137.138.47.242"
TARGET6="2001:1458:201:b50e::100:4f"
TRIES=2

ret=""

output=`ping -c $TRIES -4 -q ${TARGET4} 2>/dev/null`

ok4=$?

ret="$ret\"IPv4_ICMP_Status\": $ok4"

if [ $ok4 -eq 0 ]; then
    avg4=`echo "$output" | grep "^rtt min" | cut -d/ -f5`
    ret="$ret, \"IPv4_ICMP_avg_rtt\": $avg4"
fi

output=`ping -c $TRIES -6 -q ${TARGET6} 2>/dev/null`

ok6=$?

ret="$ret, \"IPv6_ICMP_Status\": $ok6"

if [ $ok6 -eq 0 ]; then
    avg6=`echo "$output" | grep "^rtt min" | cut -d/ -f5`
    ret="$ret, \"IPv6_ICMP_avg_rtt\": $avg6"
fi

curl -4 'http://monalisa.cern.ch/ip.php' &> /dev/null

ret="$ret, \"CURL_IPv4_Status\": $?"

curl -6 'http://monalisa.cern.ch/ip.php' &> /dev/null

ret="$ret, \"CURL_IPv6_Status\": $?"

echo "{$ret}"

exit $ok6
