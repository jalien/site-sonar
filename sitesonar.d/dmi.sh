#!/bin/bash

# Print DMI information
JSON_OUTPUT="["

DIRPATH="/sys/class/dmi/id"

parse_dir() {
	if [ $# -lt 0 ];
	then
		return
	fi

	local DIRNAME=$1

	for file in $(ls $DIRNAME)
	do
		local FILENAME=$DIRNAME/$file
		if [ -f $FILENAME ];
		then
			dmi_file=""
			output=$(cat $FILENAME 2> /dev/null)

			if [ $? -eq 0 ];
			then
				dmi_file="{ \"name\" : \"$FILENAME\", "
				dmi_file+="\"output\" : \"$output\"},"
			fi
  			JSON_OUTPUT+=$dmi_file
		fi

		if [[ -d $FILENAME && ! -L $FILENAME ]];
		then
			JSON_OUTPUT+="{ \"name\" : \"$file\", \"output\" : ["
			parse_dir $FILENAME
			JSON_OUTPUT+="]},"
		fi
	done

}

if [ ! -d $DIRPATH ]; then
	DIRPATH="/sys/devices/virtual/vmi/id"
fi

parse_dir $DIRPATH
JSON_OUTPUT+="]"
JSON_OUTPUT=$(echo $JSON_OUTPUT | sed 's/,\]/\]/g') # remove trailing comma in json
echo $JSON_OUTPUT

exit 0
