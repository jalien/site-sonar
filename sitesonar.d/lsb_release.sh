#!/bin/bash

# Print the OS description and return the exit code of lsb_release
# Tested on lxplus (CentOS 7) and Ubuntu 20.04

LSB_RELEASE=$(lsb_release -s -d | cut -d\" -f2 | xargs)

if [[ $? == 0 ]]
then
    echo "{ \"LSB_RELEASE\" : \"$LSB_RELEASE\" }"
else 
    exit_code=$?
    echo "{ \"LSB_RELEASE\" : \"\" }"
    exit $exit_code
fi
