#!/bin/bash

# Check for kernel vulnerabilites and kernel flags
JSON_OUTPUT="{ \"kernel_vulnerabilities\" : ["

for filename in $(ls /sys/devices/system/cpu/vulnerabilities)
do
  vulnerability="{ \"name\" : \"$filename\", "
  output=$(cat /sys/devices/system/cpu/vulnerabilities/$filename 2>/dev/null)
  if [ $? != 0 ]
  then
    vulnerability+="\"output\" : \"permission_error\""
  else
    vulnerability+="\"output\" : \"$output\""
  fi
  vulnerability+="},"
  JSON_OUTPUT+=$vulnerability
done

JSON_OUTPUT+="]"

JSON_OUTPUT=$(echo $JSON_OUTPUT | rev | sed '1 s/,//' | rev) # remove trailing comma in json
flags=$(cat /proc/cmdline)
JSON_OUTPUT+=" ,\"kernel_flags\" : \"$flags\"}"

echo $JSON_OUTPUT
