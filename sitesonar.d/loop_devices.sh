#!/bin/bash

# Print the loop devices

LOOP_DEVICES=$(grep "loop devices" /etc/singularity/singularity.conf)
EXITCODE=$?
if [[ $EXITCODE == 0 ]]
then
    JSON_OUTPUT="{ "
    while IFS= read -r line ; 
        do 
        key=$(echo "$line" | cut -d "=" -f 1 | xargs | tr " " _)    # split by equal mark and replace spaces with underscores
        val=$(echo "$line" | cut -d "=" -f 2- | xargs | cut -d "#" -f 1) #remove comments from the value
        
        # max_loop_devices has an integer value
        if [[ $key == "max_loop_devices" ]] 
        then
            JSON_OUTPUT+="\"$key\" : $val ,"; 
        else
            JSON_OUTPUT+="\"$key\" : \"$val\" ,"; 
        fi
    done <<< "${LOOP_DEVICES}"
    JSON_OUTPUT+=" }"

    echo $JSON_OUTPUT | rev | sed '1 s/,//' | rev
else
    echo "{ \"max_loop_devices\" : \"\" }"
    exit $EXITCODE
fi
