#!/bin/bash

# Print the batch system information

JSON_OUTPUT=

if SLURM_VERSION_STRING=$(sinfo -V 2>/dev/null)
then
    JSON_OUTPUT+=" \"type\" : \"slurm\","
    VERSION=$(echo "$SLURM_VERSION_STRING" | cut -d" " -f2)
    JSON_OUTPUT+=" \"version\" : \"$VERSION\","

elif CONDOR_VERSION=$(condor_version 2>/dev/null)
then
    JSON_OUTPUT+=" \"type\" : \"condor\","
    VERSION=$(echo "$CONDOR_VERSION" | awk 'NR == 1 { print $2 }')
    JSON_OUTPUT+=" \"version\" : \"$VERSION\","

else
    JSON_OUTPUT+=" \"type\" : \"unknown\","
    JSON_OUTPUT+=" \"version\" : \"unknown\","
fi

JSON_OUTPUT=${JSON_OUTPUT# }
JSON_OUTPUT=${JSON_OUTPUT%,}

echo "{ $JSON_OUTPUT }"

