#!/bin/bash

# Check whether the environment is running in a container

cmd='import json, os;print(json.dumps(dict(filter(lambda item: item[0].find("SING")>=0, os.environ.items()))))'

python3 -c "$cmd" | grep : && exit

echo {}
exit 1
