#!/bin/bash

# Checks if there are CPU isolated by isolcpus

ISOLATED_CPUS=$(grep isolcpus /proc/cmdline)

if [[ $? == 0 ]]; 
then
    echo "{ \"ISOLATED_CPUS\" : \"$ISOLATED_CPUS\" }"
else 
    exit_code=$?
    echo "{ \"ISOLATED_CPUS\" : \"\" }"
    exit $exit_code
fi
