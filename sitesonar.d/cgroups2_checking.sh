#!/bin/bash

# Checks if cgroups v2 is available and running

CGROUP2_FILESYSTEMS=$(grep cgroup2 /proc/filesystems)

if [ ! -z "$CGROUP2_FILESYSTEMS" ]
then
    JSON_OUTPUT="{ \"CGROUPSv2_AVAILABLE\" : true "
    #CGROUP2_MOUNTS=$(cat /sys/fs/cgroup/cgroup.controllers)
   # if [ ! -z "$CGROUP2_MOUNTS" ]
    if [ -f /sys/fs/cgroup/cgroup.controllers ]
    then
        JSON_OUTPUT+=", \"CGROUPSv2_RUNNING\" : true "
    	controllers=$(cat /sys/fs/cgroup/cgroup.controllers)
	JSON_OUTPUT+=", \"CGROUPSv2_CONTROLLERS\" : \"$controllers\" } " 
    else
        JSON_OUTPUT+=", \"CGROUPSv2_RUNNING\" : false "
	JSON_OUTPUT+=", \"CGROUPSv2_CONTROLLERS\" : \"disabled\" } "
    fi
else
    JSON_OUTPUT="{ \"CGROUPSv2_AVAILABLE\" : false , \"CGROUPSv2_RUNNING\" : false , \"CGROUPSv2_CONTROLLERS\" : \"disabled\" }"
fi

echo "$JSON_OUTPUT"
