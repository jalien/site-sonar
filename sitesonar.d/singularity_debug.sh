#!/bin/bash

export XDG_RUNTIME_DIR=/run/user/$(id -u)
export DBUS_SESSION_BUS_ADDRESS="unix:path=${XDG_RUNTIME_DIR}/bus"

# Print whether Apptainer / Singularity is supported

SINGULARITY_EXEC="singularity exec -B /cvmfs:/cvmfs "
SINGULARITY_CVMFS_EXEC="/cvmfs/alice.cern.ch/containers/bin/apptainer/current/bin/"$SINGULARITY_EXEC

TEST_CMD="/cvmfs/alice.cern.ch/containers/fs/singularity/centos7-alice-20211122 java -version"

CHECK_LOCAL=$($SINGULARITY_EXEC $TEST_CMD 2>&1 | grep -o "Runtime")
CHECK_CVMFS=$($SINGULARITY_CVMFS_EXEC $TEST_CMD 2>&1 | grep -o "Runtime")
CHECK_CGROUPS2=$($SINGULARITY_CVMFS_EXEC --memory 10M --memory-swap 0 $TEST_CMD 2>&1 | grep -o "Runtime")

if [ -z "$CHECK_LOCAL" ]
  then
      SINGULARITY_LOCAL_SUPPORTED=false
  else
      SINGULARITY_LOCAL_SUPPORTED=true
fi

if [ -z "$CHECK_CVMFS" ]
  then
      SINGULARITY_CVMFS_SUPPORTED=false
  else
      SINGULARITY_CVMFS_SUPPORTED=true
fi

if [ -z "$CHECK_CGROUPS2" ]
  then
      SINGULARITY_CGROUPS2_SUPPORTED=false
  else
      SINGULARITY_CGROUPS2_SUPPORTED=true
fi

echo "{ \
\"SINGULARITY_LOCAL_SUPPORTED\"    : $SINGULARITY_LOCAL_SUPPORTED, \
\"SINGULARITY_CVMFS_SUPPORTED\"    : $SINGULARITY_CVMFS_SUPPORTED, \
\"SINGULARITY_CGROUPS2_SUPPORTED\" : $SINGULARITY_CGROUPS2_SUPPORTED }"
