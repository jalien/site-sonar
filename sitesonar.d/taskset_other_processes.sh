#!/bin/bash

# Checks if any cpu isolated

function compute_percentage {
availcpu="$2" processesmask="$1" python - <<END

def find_processors(s, ch):
    s=s[::-1]
    return [i for i, ltr in enumerate(s) if ltr == ch]


def unique(list1):
    # intilize a null list
    unique_list = []

    # traverse for all elements
    for x in list1:
        # check if exists in unique_list or not
        if x not in unique_list:
            unique_list.append(x)
    return unique_list

import sys
import os
availcpus = int(os.environ['availcpu'])
processesmask = os.environ['processesmask']
processesmask = processesmask.replace('-', ' ')

fullmask=''.join([char*int(availcpus) for char in '1'])
fullmask = '0b' + fullmask

if availcpus == 0:
    availcpus = 255

listmask = processesmask.split()
listmask = [bin(int(x, base=16)) for x in listmask]
pinnedcpus = []
for mask in listmask:
    if mask != fullmask:
        mask_pinned = find_processors(mask, '1')
        pinnedcpus.extend(mask_pinned)
pinnedcpus = len(unique(pinnedcpus))
mask_pinned=pinnedcpus
percentage = pinnedcpus / availcpus
percentage=percentage*100
percentage="{:.2f}".format(percentage)

print(percentage)
END
}
entries=$(ps aux | grep -v "^root" | tr -s " " | cut -d" " -f2 | xargs -L1  taskset -p 2> /dev/null | cut -d " " -f 6 | uniq)
count=$(echo $entries | wc -w)
entriesDashed=`echo "$entries" | tr '\n' '-'`
availcpus=`grep -c "^processor" /proc/cpuinfo`
percentage=`compute_percentage $entriesDashed $availcpus`

JSON_OUTPUT="{ \"ENTRIES\" : \"$entries\", "
JSON_OUTPUT+="\"ENTRY_COUNT\" : $count, "
JSON_OUTPUT+="\"PERCENTAGE_PINNED_CPUS\" : ${percentage:-0} } "
echo $JSON_OUTPUT
