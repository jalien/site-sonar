#!/bin/bash

print_filesystem () {
	fsname=$(echo $x | cut -d " " -f 1)
	resultarr+="\"FILESYSTEM_NAME\" : \"$fsname\""
	fssize=$(echo $x | cut -d " " -f 2)
	resultarr+=", \"SIZE\" : $fssize"
	fsavailable=$(echo $x | cut -d " " -f 4)
	resultarr+=", \"AVAILABLE\" : $fsavailable"
	mountpoint=$(echo $x | cut -d " " -f 6)
        resultarr+=", \"MOUNTPOINT\" : \"$mountpoint\""
        filesystype=`mount | awk -v mountp=$mountpoint '{if ($3 == mountp) {print $5}}'`
        resultarr+=", \"MOUNTPOINT_TYPE\" : \"$filesystype\""
	echo $resultarr
}

JSON_OUTPUT=""

logical_cores_count=`lscpu | grep "^CPU(s):" | cut -d":" -f2 | xargs`
total_disk=`df -P . | tail -n+2 | awk '{print $2;}'`
disk_per_core=$(bc <<<"scale=2;$total_disk/$logical_cores_count/1024/1024")
JSON_OUTPUT+="{\"TOTAL_DISK_PER_CORE_GB\" : $disk_per_core, "

JSON_OUTPUT+=" \"ALARMING\" : $(echo "$disk_per_core < 10" |bc -l), "

while read x; do
	ourfilesystem="`print_filesystem`"
done < <(df -P . | tail -n+2)
JSON_OUTPUT+=" \"OUR_FILESYSTEM\" : { $ourfilesystem }, "

fullarray=" \"ALL_FILESYSTEMS\" : [{"
while read x; do 
	filesysresult="`print_filesystem`"
	fullarray="$fullarray$filesysresult }, {";
done < <(df -P | tail -n+2)
JSON_OUTPUT+=" ${fullarray::-4}}]}"

echo $JSON_OUTPUT

if (( $(echo "$disk_per_core < 10" |bc -l) )); then
	exit 255
fi

exit $?
