#!/bin/bash

# Print CVMFS version, revision and crucial parameters

JSON_OUTPUT=

repo=alice.cern.ch

# example outputs:
# CVMFS_CACHE_REFCOUNT=true    # from /cvmfs/config-egi.egi.eu/etc/cvmfs/config.d/alice.cern.ch.conf
# CVMFS_NFILES=524288    # from /cvmfs/config-egi.egi.eu/etc/cvmfs/config.d/alice.cern.ch.conf

for i in CACHE_REFCOUNT NFILES
do
    key=CVMFS_$i
    val=$(
        cvmfs_config showconfig $repo | perl -ne '
            s/^'"$key"'=(\w+).*/$1/ && print
        '
    )

    JSON_OUTPUT+=" \"$key\" : \"$val\","
done

key=CVMFS_VERSION
val=$(attr -qg version /cvmfs/$repo)

JSON_OUTPUT+=" \"$key\" : \"$val\","

key=CVMFS_REVISION
val=$(attr -qg revision /cvmfs/$repo)
exit_code=$?

if [[ $exit_code == 0 ]]
then
    case $val in
    ''|*[!0-9]*)
	echo "$0: illegal revision: '$val'" >&2
	;;
    *)
	JSON_OUTPUT+=" \"$key\" : $val,"
    esac
fi

JSON_OUTPUT=${JSON_OUTPUT# }
JSON_OUTPUT=${JSON_OUTPUT%,}

echo "{ $JSON_OUTPUT }"

exit $exit_code
