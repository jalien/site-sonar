#! /bin/bash

LIST=("Virtualization:" "Hypervisor vendor:" "Virtualization type:")

JSON_OUTPUT="{"
CONTENT="$(lscpu)"

for feature in "${LIST[@]}"
do
	output=$(echo "$CONTENT" | grep "$feature" | sed "s/$feature//g")
	if [[ -z $output ]];
	then
		output="N/A"
	fi

	JSON_OUTPUT+=" \"$feature\": \"$output\", "

done

JSON_OUTPUT+="}"
JSON_OUTPUT=$(echo $JSON_OUTPUT | sed 's/, }$/}/')

echo "$JSON_OUTPUT"
