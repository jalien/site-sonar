#!/bin/bash

# Print the RAM information

MEM_INFO=$(cat /proc/meminfo)

JSON_OUTPUT="{ "
while IFS= read -r line ; 
    do 
    key=$(echo "$line" | cut -d ":" -f 1)
	val=$(echo "$line" | cut -d ":" -f 2- | xargs)
    if [[ $key == "HugePages_Total" || $key == "HugePages_Free" || $key == "HugePages_Rsvd" || $key == "HugePages_Surp" ]] 
    then
        JSON_OUTPUT+="\"RAM_$key\" : $val," # These keys do not have kB suffix
    else
        JSON_OUTPUT+="\"RAM_kB_$key\" : ${val::-3}," #last 3 characters are removed because it is kB suffix
    fi
done <<< "${MEM_INFO}"

SWAPPINESS_INFO=$(cat /proc/sys/vm/swappiness)
JSON_OUTPUT+="\"SWAPPINESS\" : $SWAPPINESS_INFO,"

JSON_OUTPUT+=" }"

echo $JSON_OUTPUT | rev | sed '1 s/,//' | rev
